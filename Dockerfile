FROM cypress/base:10.18.0

RUN mkdir /app
WORKDIR /app

COPY . /app

#RUN apt-get update && apt-get upgrade -y \
#&& apt install curl

RUN npm install

RUN $(npm bin)/cypress verify

# RUN $(npm bin)/cypress run 
RUN npm run runHeadless
